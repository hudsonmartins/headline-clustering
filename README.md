This project has the goal of given a collection of news headlines, produce meaningful clusters of headlines. 
In other words, cluster them in such a way that each cluster has a common “theme”.
The dataset used on this project is available at: https://www.ic.unicamp.br/~rocha/teaching/2018s1/mo444/assignments/assignment-03/